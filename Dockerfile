# syntax=docker/dockerfile:1
#  ┬─┐┌─┐┌─┐┬┌ ┬─┐┬─┐┬─┐o┬  ┬─┐
#  │ ││ ││  ├┴┐├─ │┬┘├─ ││  ├─ 
#  ┘─┘┘─┘└─┘┘ ┘┴─┘┘└┘┘  ┘┘─┘┴─┘

ARG REDIS_TAG=7.2.4-alpine
FROM redis:$REDIS_TAG
ENTRYPOINT /entrypoint.sh

COPY entrypoint.sh /entrypoint.sh
COPY redis.conf /redis.conf
COPY redis.conf.default /redis.conf.default
RUN chmod 0644 /redis.conf* && \
		chmod 0755 /entrypoint.sh

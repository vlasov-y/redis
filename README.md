Redis for replication
===

Official Redis image, with simple config and couple environment variables in order to setup replication.

## Environment variables

| Name              | Mode   | Default     | Description                                                                                         |
| ----------------- | ------ | ----------- | --------------------------------------------------------------------------------------------------- |
| MAX_MEMORY        | both   | 100mb       | Max RAM limit to use                                                                                |
| CONFIG            | both   | /redis.conf | Path to main redis config                                                                           |
| REQUIRE_PASS      | master | *empty*     | Password for AUTH                                                                                   |
| REQUIRE_PASS_FILE | master | *empty*     | Path to the file that contains password if you do not want to pass the password as the env variable |
| REPLICA_OF        | slave  | *empty*     | IP:PORT to master Redis instance                                                                    |
| REPLICA_ANNOUNCE  | slave  | *empty*     | Public IP:PORT of slave which must be accessible from master                                        |
| MASTERAUTH        | slave  | *empty*     | Password for connection to master                                                                   |
| MASTERAUTH_FILE   | slave  | *empty*     | Path to the file that contains password if you do not want to pass the password as the env variable |
| SHOW_CONFIG_DIFF  | both   | *empty*     | Set to any value in order to see the diff between default and running config in the log             |

## Examples

```yaml
services:
  master:
    image: yuriyvlasov/redis
    environment:
      REQUIRE_PASS: qwer1234
  slave:
    image: yuriyvlasov/redis
    depends_on:
      - master
    environment:
      MASTERAUTH: qwer1234
      REPLICA_OF: master:6379
```

## Changelog

*27 Feb 2023*

- Added REQUIRE_PASS_FILE
- Added MASTERAUTH_FILE
- Added SHOW_CONFIG_DIFF
- Added pre-commit
- Refactored entrypoint.sh
- Added redis.conf.default from Redis v7.0

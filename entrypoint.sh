#!/usr/bin/env sh
# shellcheck disable=SC3060,SC2250,SC2154,SC2269,SC2312
set -e

# set random passwords - https://talosintelligence.com/vulnerability_reports/TALOS-2019-0782
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
cut -d: -f1 /etc/shadow | \
while IFS= read -r USER; do
	P="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 64)"
	printf "%s:%s" "$USER" "$P" | chpasswd 1>/dev/null 2>&1
done

MAX_MEMORY="${MAX_MEMORY:-100mb}"
CONFIG="${CONFIG:-/redis.conf}"
REQUIRE_PASS="$REQUIRE_PASS"
MASTERAUTH="$MASTERAUTH"

# password
if [ -f "$REQUIRE_PASS_FILE" ] && [ -s "$REQUIRE_PASS_FILE" ]; then
	echo "info: reading REQUIRE_PASS from $REQUIRE_PASS_FILE"
	REQUIRE_PASS="$(tr -d '[:space:]' <"$REQUIRE_PASS_FILE")"
fi
if [ "$REQUIRE_PASS" != '' ]; then
	sed -ri "s:^#\srequirepass.+:requirepass ${REQUIRE_PASS//:/\\:}:g" "$CONFIG"
fi
# max memory
sed -ri "s:^#\smaxmemory.+:maxmemory ${MAX_MEMORY//:/\\:}:g" "$CONFIG"
# replica settings
if [ "$REPLICA_OF" != '' ]; then
	sed -ri "s:^#\sreplicaof.+:replicaof $(echo "$REPLICA_OF" | tr : " "):g" "$CONFIG"
	if [ -f "$MASTERAUTH_FILE" ] && [ -s "$MASTERAUTH_FILE" ]; then
		echo "info: reading MASTERAUTH from $MASTERAUTH_FILE"
		MASTERAUTH="$(tr -d '[:space:]' <"$MASTERAUTH_FILE")"
	fi
	if [ "$MASTERAUTH" != '' ]; then
		sed -ri "s:^#\smasterauth.+:masterauth ${MASTERAUTH//:/\\:}:g" "$CONFIG"
	fi
	if [ "$REPLICA_ANNOUNCE" != '' ]; then
		sed -ri "s:^#\replica-announce-ip.+:replica-announce-ip $(echo "$REPLICA_ANNOUNCE" | cut -d: -f1):g" "$CONFIG"
		sed -ri "s:^#\replica-announce-port.+:replica-announce-port $(echo "$REPLICA_ANNOUNCE" | cut -d: -f2):g" "$CONFIG"
	fi
fi

if [ -n "$SHOW_CONFIG_DIFF" ]; then
	diff -bBU0 /redis.conf.default /redis.conf | \
	sed -r "s:${REQUIRE_PASS//:/\\:}:<password is hidden>:g; s:${MASTERAUTH//:/\\:}:<password is hidden>:g;" 2>/dev/null || true
fi

redis-server /redis.conf
